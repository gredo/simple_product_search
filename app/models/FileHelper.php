<?php

namespace models;


/**
 * Description of FileHelper
 *
 * @author Lukas Krebs
 */
class FileHelper
{


    /**
     * Gets lines of text from file
     *
     * @param string $sPath Path to file
     * @return array|FALSE array on success, FALSE otherwise
     */
    public function getLinesFromFile($sPath)
    {
        return file($sPath, FILE_SKIP_EMPTY_LINES);
    }


    /**
     * Appends line to file
     *
     * @param string $sPath file to path
     * @param string $sLine appended line
     * @return boolean TRUE if done successfully, FALSE otherwise
     */
    public function fileAppend($sPath, $sLine)
    {
        $fp = fopen($sPath, 'a+');//appending
        if(FALSE === $fp){
            return FALSE;
        }

        //write to the file, prevent collissions
        if (flock($fp, LOCK_EX)) {
            fwrite($fp, $sLine);
            flock($fp, LOCK_UN); // unlock the file
        } else {
            //no lock obtained
            return FALSE;
        }

        return fclose($fp);
    }


    /**
     * Replaces file content
     *
     * @param string $sPath Path to file
     * @param string $sContent replacing content
     * @return boolean TRUE on success, FALSE otherwise
     */
    public function replaceFileContent($sPath, $sContent)
    {
        $fp = fopen($sPath, 'w');//writing
        if(FALSE === $fp){
            return FALSE;
        }

        //write to the file, prevent collissions
        if (flock($fp, LOCK_EX)) {
            fwrite($fp, $sContent);//write to the file
            flock($fp, LOCK_UN); // unlock the file
        } else {
            //no lock obtained
            return FALSE;
        }

        return fclose($fp);
    }
}
