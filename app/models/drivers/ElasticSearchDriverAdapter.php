<?php

namespace models\drivers;


/**
 * Implements IDriver interface over ElasticSearchDriver
 *
 * @author Lukas Krebs
 */
class ElasticSearchDriverAdapter implements IDriver
{


    /**
     *
     * @var IElasticSearchDriver
     */
    private $driver;


    public function __construct(IElasticSearchDriver $elasticSearchDriver)
    {
        $this->driver = $elasticSearchDriver;
    }


    public function findById($id)
    {
        return $this->driver->findById($id);
    }

}
