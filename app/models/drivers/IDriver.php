<?php

namespace models\drivers;


/**
 * Common interface for both mysql and elasticsearch.
 *
 * @author Lukas Krebs
 */
interface IDriver {


    /**
     * @param string $id
     * @return array
     */
    public function findById($id);
}
