<?php

namespace models\drivers;


/**
 * Implements IDriver interface over MysqlDriver
 *
 * @author Lukas Krebs
 */
class MysqlDriverAdapter implements IDriver
{


    /**
     *
     * @var IMySQLDriver
     */
    private $mysqlDriver;


    /**
     *
     * @param IMySQLDriver $mysqlDriver
     */
    public function __construct(IMySQLDriver $mysqlDriver)
    {
        $this->mysqlDriver = $mysqlDriver;
    }



    public function findById($id)
    {
        return $this->mysqlDriver->findProduct($id);
    }

}
