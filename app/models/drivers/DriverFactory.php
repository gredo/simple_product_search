<?php

namespace models\drivers;


/**
 * Choose and create correct driver for given database type
 *
 * @author Lukas Krebs
 */
class DriverFactory
{

    const ELASTIC = 'elastic';
    const MYSQL = 'mysql';

    /**
     *
     * @var string
     */
    private $dbtype;

    private $esConn;
    private $mysqlConn;


    /**
     *
     * @param type $esConn
     * @param type $mysqlConn
     * @param string $dbtype what connection should be used
     */
    public function __construct($esConn, $mysqlConn, $dbtype)
    {
        $this->dbtype = $dbtype;
        $this->esConn = $esConn;
        $this->mysqlConn = $mysqlConn;
    }


    /**
     * Loads correct driver from given dbtype
     *
     * @return IDriver
     */
    public function create()
    {
        switch($this->dbtype){
            case self::ELASTIC:
                return new ElasticSearchDriverAdapter(new ElasticSearchDriver($this->esConn));
            case self::MYSQL:
                return new MysqlDriverAdapter(new MysqlDriver($this->mysqlConn));
        }
    }
}
