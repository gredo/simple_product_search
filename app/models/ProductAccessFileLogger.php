<?php

namespace models;


/**
 * Logging product usage to file
 *
 * @author Lukas Krebs
 */
class ProductAccessFileLogger implements IProductAccessLogger
{

    const PROD_CTR_PATH = LOG . '\product_counter.txt';//@todo get to config file and inject it?
    const FILE_SEP = ';';


    /**
     *
     * @var FileHelper
     */
    private $fileHelper;


    public function __construct()
    {
        $this->fileHelper = new FileHelper();
    }


    /**
     * Update product usage counter.
     *
     * @param type $id_product Id of product
     * @return boolean TRUE if successfull, FALSE otherwise
     */
    public function updateCounter($id_product)
    {
        //get all lines from file
        $aLines = $this->fileHelper->getLinesFromFile(self::PROD_CTR_PATH);//return array of lines
        if(FALSE === $aLines){
            return FALSE;
        }

        //find line with product counter.
        //if product is found, update the counter
        $found = FALSE;
        foreach ($aLines as $line_num => &$line) {
            $aLine = explode(self::FILE_SEP, $line);
            if(count($aLine) == 2){
                if((int)$aLine[0] === (int) $id_product){
                    $line  = $this->createLogLine($id_product, ((int)$aLine[1])+1);
                    $found = TRUE;
                    break;
                }
            }
        }

        //if product is found and counter updated, save to file
        if($found){
            $this->fileHelper->replaceFileContent(self::PROD_CTR_PATH, implode("", $aLines));
        }
        //otherwise just append new line
        else{
            $this->fileHelper->fileAppend(self::PROD_CTR_PATH, $this->createLogLine($id_product));
        }

        return TRUE;
    }


    /**
     * Creates the log line in proper format
     *
     * @param int $id_product Id of product
     * @param int $value value to be logged. Default value is 1
     * @return string
     */
    private function createLogLine($id_product, $value = 1)
    {
        return $id_product . self::FILE_SEP .$value . PHP_EOL;
    }

}
