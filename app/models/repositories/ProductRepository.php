<?php

namespace models\repositories;

use models\drivers\IDriver;
use Psr\Cache\CacheItemPoolInterface;


/**
 * Description of ProductRepository
 *
 * @author Lukas Krebs
 */
class ProductRepository
{

    /**
     *
     * @var CacheItemPoolInterface
     */
    private $cacheItemPool;

    /**
     *
     * @var IDriver
     */
    private $driver;



    public function __construct(CacheItemPoolInterface $cacheItemPool, IDriver $driver)
    {
        $this->cacheItemPool = $cacheItemPool;
        $this->driver = $driver;
    }



    /**
     *
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function findById($id)
    {
        //initialize product cache, get data from cache
        $productCache = $this->cacheItemPool->getItem('Product_' . $id);

        //if data are not cached, load them from database and store to cache
        if( ! $productCache->isHit()){
            //otherwise get data from database
            try{
                $aProduct = $this->driver->findById($id);
                //handling bad results
                if( ! is_array($aProduct)){
                    throw new \Exception();
                }

                //store to cache
                $productCache->set($aProduct);
                $this->cacheItemPool->save($productCache);
            }catch(\Exception $e){
                //send the exception to controller
                throw $e;
            }

        }

        //return data
        return $productCache->get();
    }

}