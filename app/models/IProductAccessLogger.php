<?php

namespace models;


/**
 * Logging product usage
 *
 * @author Lukas Krebs
 */
interface IProductAccessLogger {



    /**
     * Update counter of product usage.
     *
     * @param type $id_product Id of product
     * @return boolean TRUE if successfull, FALSE otherwise
     */
    public function updateCounter($id_product);

}
