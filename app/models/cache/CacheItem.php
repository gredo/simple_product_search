<?php

namespace models\cache;


/**
 * This object represents one cached thing from cache pool.
 * @todo Should be reimplemented.
 *
 * @author Lukas Krebs
 */
class CacheItem implements \Psr\Cache\CacheItemInterface
{

    private $key;
    private $val;


    public function __construct($key, $val = NULL)
    {
        $this->key = $key;
        $this->val = $val;
    }


    public function expiresAfter($time)
    {
        //not needed
    }

    public function expiresAt($expiration)
    {
        //not needed
    }

    public function get()
    {
        return $this->val;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function isHit()
    {
        return (NULL === $this->val) ? FALSE : TRUE;
    }

    public function set($value)
    {
        $this->val = $value;
    }

}
