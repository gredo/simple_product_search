<?php

namespace models\cache;


/**
 * This object handles caching of data.
 * @todo needs to be implemented. Does not actually do anything now.
 *
 * @author Lukas Krebs
 */
class FileCacheItemPool implements \Psr\Cache\CacheItemPoolInterface
{


    public function clear()
    {

    }

    public function commit()
    {

    }

    public function deleteItem($key)
    {

    }

    public function deleteItems(array $keys)
    {

    }

    public function getItem($key)
    {
        return new CacheItem($key);
    }

    public function getItems(array $keys = array())
    {

    }

    public function hasItem($key)
    {

    }

    public function save(\Psr\Cache\CacheItemInterface $item)
    {

    }

    public function saveDeferred(\Psr\Cache\CacheItemInterface $item)
    {

    }

}
