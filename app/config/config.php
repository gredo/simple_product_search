<?php

/**
 * Config file
 *
 * @author Lukas Krebs
 */


//constants
const APP = __DIR__ . "\..\..\app";//path to app directory
const LOG = __DIR__ . "\..\..\log";//path to log directory


/** What connection should be used */
$config['dbType']    = models\drivers\DriverFactory::MYSQL;

/**
 * Connection to mysql; normally there would be connection parameters and some object would create connection from it; this is simplification
 */
$config['mysqlConn'] = 'some conn to mysql';
$config['esConn']    = 'some conn to elastic';