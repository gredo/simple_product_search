<?php

namespace Controllers;

use models\repositories\ProductRepository;
use models\IProductAccessLogger;


/**
 *
 * @author Lukas Krebs
 */
class ProductController
{

    /**
     *
     * @var ProductRepository
     */
    private $productRepository;


    /**
     *
     * @var IProductAccessLogger
     */
    private $productAccessLogger;



    /**
     *
     * @param ProductRepository $pr
     * @param \Controllers\ProductAccessLogger $pal
     */
    public function __construct(ProductRepository $pr, IProductAccessLogger $pal)
    {
        $this->productRepository = $pr;
        $this->productAccessLogger = $pal;
    }


    /**
     * @param string $id
     * @return string|FALSE
     */
    public function detail($id)
    {

        try{
            //get product
            $aProduct = $this->productRepository->findById($id);

            //logging access to product
            if(is_array($aProduct)){
                $this->productAccessLogger->updateCounter($id);
            }
        }catch(\Exception $e){
            //here comes exception handling
            return FALSE;//for now just return FALSE
        }

        //return product in JSON
        return json_encode($aProduct);
    }

}
