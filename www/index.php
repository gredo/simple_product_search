<?php

/**
 *
 * For testing without framework.
 *
 * @author Lukas Krebs
 *
 *
 */

//handle autoloading
require __DIR__ . '/../vendor/autoload.php';


//config file loading
include __DIR__ . '/../app/config/config.php';



//init cacheItemPool; it can be easily switched with another cache pool
$cacheItemPool     = new \models\cache\FileCacheItemPool();

//get proper driver; changing $config['dbType'] changes database
$driverFactory     = new models\drivers\DriverFactory($config['esConn'], $config['mysqlConn'], $config['dbType']);

//init productRepository
$productRepository = new models\repositories\ProductRepository($cacheItemPool, $driverFactory->create());

//init product access logger; here it can be changed to something else in future
$prodAccLogger     = new models\ProductAccessFileLogger();
$productController = new \Controllers\ProductController($productRepository, $prodAccLogger);

//for testing
//echo $productController->detail(5);
//echo $productController->detail(6);
//echo $productController->detail(4);
//echo $productController->detail(1);
